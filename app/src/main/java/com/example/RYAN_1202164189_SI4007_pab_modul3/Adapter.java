package com.example.RYAN_1202164189_SI4007_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.RYAN_1202164233_SI4007_pab_modul3.R;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private List<Orang> orangList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView namanya, jabatan;
        public ImageView foto;
        public CardView cvItem;

        public MyViewHolder(View view) {
            super(view);
            namanya = (TextView) view.findViewById(R.id.namanya);
            jabatan = (TextView) view.findViewById(R.id.jabatan);
            foto = view.findViewById(R.id.foto);
            cvItem = view.findViewById(R.id.cv_item);
        }
    }


    public Adapter(Context context, List<Orang> orangList) {
        this.context = context;
        this.orangList = orangList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Orang orang= orangList.get(position);
        holder.jabatan.setText(orang.job);
        holder.namanya.setText(orang.nama);
        if (orang.getFoto() == 0) {
            holder.foto.setImageDrawable(context.getResources().getDrawable(R.drawable.boy));
        } else {
            holder.foto.setImageDrawable(context.getResources().getDrawable(R.drawable.girl));
        }
        holder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Detail.class);
                intent.putExtra("data", orang);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.orangList.size();
    }
}