package com.example.RYAN_1202164189_SI4007_pab_modul3;

import android.content.res.Configuration;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.RYAN_1202164233_SI4007_pab_modul3.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Orang> listOrang = new ArrayList<Orang>();
    private Adapter adapterOrang;
    private String[] jenkel = new String[]{"Male", "Female"};
    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.adapterOrang = new Adapter(this, listOrang);
        rv = findViewById(R.id.recyclerview);
        rv.setAdapter(adapterOrang);
        rv.setLayoutManager(new LinearLayoutManager(this));
    }

    public void fabListener(View view){
        final AlertDialog.Builder builder =  new AlertDialog.Builder(MainActivity.this);
        final View dialog = this.getLayoutInflater().inflate(R.layout.design_dialog, null);
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, jenkel);
        Spinner s = dialog.findViewById(R.id.spinner);
        s.setAdapter(adapter);

        final EditText etNama = dialog.findViewById(R.id.editText_nama);
        final EditText etPekerjaan = dialog.findViewById(R.id.editText_pekerjaan);
        Button btnSubmit = dialog.findViewById(R.id.submit);
        Button btnCancel = dialog.findViewById(R.id.cancel);
        builder.setView(dialog);
        final AlertDialog ad = builder.show();

        final Orang o = new Orang();

        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                o.setFoto(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = etNama.getText().toString();
                String pekerjaan = etPekerjaan.getText().toString();

                o.setNama(nama);
                o.setJob(pekerjaan);

                listOrang.add(o);
                ad.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){
            rv.setLayoutManager(new GridLayoutManager(this, 2));
        }else if (newConfig.orientation==Configuration.ORIENTATION_PORTRAIT){
            rv.setLayoutManager(new LinearLayoutManager(this));
        }
    }
}
