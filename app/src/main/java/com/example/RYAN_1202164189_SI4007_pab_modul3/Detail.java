package com.example.RYAN_1202164189_SI4007_pab_modul3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.RYAN_1202164233_SI4007_pab_modul3.R;

public class Detail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ImageView foto = findViewById(R.id.foto);
        TextView tvNama = findViewById(R.id.txt_nama);
        TextView tvPekerjaan = findViewById(R.id.txt_job);

        Intent dataIntent = getIntent();
        Orang o = dataIntent.getParcelableExtra("data");

        if (o.getFoto() == 0) {
            foto.setImageDrawable(getResources().getDrawable(R.drawable.boy));
        } else {
            foto.setImageDrawable(getResources().getDrawable(R.drawable.girl));
        }

        tvNama.setText(o.getNama());
        tvPekerjaan.setText(o.getJob());
    }
}
